from fastapi import APIRouter, Depends, Path, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import List, Optional

from config.database import Session
from models.movie import Movie as MovieModel # Para que no se confunda con el Model de esta página
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()

# Al llamar a esta ruta, se debe enviar el token de autenticación
@movie_router.get('/movies', tags=["movies"], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=["movies"], response_model=Movie)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Película no encontrada"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

# http://localhost:8000/movies?category=Acción
# Con parámetro query
@movie_router.get('/movies/', tags=["movies"], response_model=List[Movie])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.post('/movies', tags=["movies"], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
    db = Session() # Se crea la sesión con la base de datos
    MovieService(db).create_movie(movie) 
    return JSONResponse(status_code=201, content={"message": "Película creada"})

# Con parametro de ruta {id}, se debe enviar en la url
# Actualizar una película
@movie_router.put('/movies/{id}', tags=["movies"], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Película no encontrada"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "Película modificada"})

@movie_router.delete('/movies/{id}', tags=["movies"], response_model=dict ,status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Película no encontrada"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message": "Película eliminada"})